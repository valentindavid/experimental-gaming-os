BST=bst
OSTREE_BRANCH=gaming/master
LOCAL_ADDRESS=$(shell ip route get 1.1.1.1 | cut -d" " -f7)

all: ostree-repo checkout/disk.qcow2

result/flatpak-with-steam.tar.gz:
	$(BST) build flatpak/flatpak-installer.bst
	[ -d result ] || mkdir result
	rm -rf temp
	mkdir temp
	mkdir temp/tmp temp/vartmp temp/flatpak
	$(BST) shell					\
	    --mount temp/tmp /tmp			\
	    --mount temp/vartmp /var/tmp		\
	    --mount temp/flatpak /var/lib/flatpak	\
	    --mount result /result			\
	    flatpak/flatpak-installer.bst		\
	    /usr/bin/install-steam.sh

checkout/disk.qcow2: ostree-config.yml files/ostree-config/gaming.gpg ostree-gpg result/flatpak-with-steam.tar.gz
	rm -rf checkout
	$(BST) build image/image.bst
	$(BST) checkout image/image.bst checkout

update-repo: ostree-config.yml files/ostree-config/gaming.gpg ostree-gpg
	env BST="$(BST)" utils/update-repo.sh		\
	  --gpg-homedir=ostree-gpg			\
	  --gpg-sign=$$(cat ostree-gpg/default-id)	\
	  --collection-id=org.freedesktop.Gaming	\
	  ostree-repo image/repo.bst			\
	  $(OSTREE_BRANCH)

udpate-steam:
	rm result/flatpak-with-steam.tar.gz
	$(MAKE) result/flatpak-with-steam.tar.gz

files/ostree-config/gaming.gpg: ostree-gpg
	gpg --homedir=ostree-gpg --export --armor >"$@"


ostree-config.yml:
	echo 'ostree-remote-url: "http://$(LOCAL_ADDRESS):8000/"' >"$@.tmp"
	echo 'ostree-branch: "$(OSTREE_BRANCH)"' >>"$@.tmp"
	mv "$@.tmp" "$@"

define OSTREE_GPG_CONFIG
Key-Type: DSA
Key-Length: 1024
Subkey-Type: ELG-E
Subkey-Length: 1024
Name-Real: OSTree Gaming TEST
Expire-Date: 0
%no-protection
%commit
%echo finished
endef

export OSTREE_GPG_CONFIG
ostree-gpg:
	rm -rf ostree-gpg.tmp
	mkdir ostree-gpg.tmp
	chmod 0700 ostree-gpg.tmp
	echo "$${OSTREE_GPG_CONFIG}" >ostree-gpg.tmp/key-config
	gpg --batch --homedir=ostree-gpg.tmp --generate-key ostree-gpg.tmp/key-config
	gpg --homedir=ostree-gpg.tmp -k --with-colons | sed '/^fpr:/q;d' | cut -d: -f10 >ostree-gpg.tmp/default-id
	mv ostree-gpg.tmp ostree-gpg

ostree-repo:
	$(MAKE) update-repo

ostree-serve:
	webfsd -F -l - -p 8000 -r ostree-repo

.PHONY: update-repo clean
