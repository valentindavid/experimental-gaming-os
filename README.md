# Experimental Gaming OS (name to be changed)

This is an experimental OS image running Steam client in full
screen. The compositore is Gamescope. This is intended for couch
gaming PCs.

The OS is built on top of Freedesktop SDK. Updates can be rolled with
OSTree. Steam is installed as a Flatpak.

## Requirements

### The builder

The builder needs to be an x86_64. You will need BuildStream 1.6.2 and
bst-external 0.23.1. To server the OSTree updates, you will need
webfsd, or any other web server you can configure yourself.

### The gaming machine

The gaming machine needs to be an x86_64. It must use a GPU that is
working on Mesa, for example Intel and AMD GPUs. It needs to be able
to boot in UEFI without secure boot.

## Installing the OS

### Build the image

```
make checkout/disk.qcow2
```

### Installation

For the moment the image does not resize itself, if you device is `/dev/sdz`

```
$ sudo qemu-img dd if=checkout/disk.qcow2 of=/dev/sdz
$ echo ", +" | sudo sfdisk /dev/sdz -N 2
$ sudo resize2fs /dev/sdz2
```

Install the disk in the device, then boot it using UEFI with secure
boot disabled.

### First boot

Go on the VT, for example VT2 with `ctrl-alt-f2`. Login as `root`,
password `root`.

```
# passwd root
[Setup your new root password]
# hostnamectl hostname my-new-machine-name
# localectl set-x11-keymap [your key map]
# localectl set-keymap [your key map]
# localectl set-locale [your locale]
```

If you use Wifi or a network without DHCP, you will need to setup your
network using `nmcli` (NetworkManager's console client).

You can return to Steam with with `ctrl-alt-f1`.

### Updating

On builder run:
```
$ make update-ostree
$ make ostree-serve
```

Remember the your directory `ostree-gpg` that contains the private
signing keys have to be kept. Otherwise you will need to manually
install a new public key on your gaming machine.

(Note you can run your own web server instead of `make
ostree-serve`. Just serve out directory `ostree-repo` to port 8000 on
HTTP.

On the gaming computer:
```
# ostree pull gaming gaming/master
# ostree admin deploy gaming:gaming/master
```

Also update Flatpak applications:

```
# flatpak update
```

Then reboot:

```
# systemctl reboot
```

### Issues

#### Steam does not start

First few starts of Steam might not work. Try restarting it with:

```
# systemctl restart steam
```

#### My resolution is not 1080p

Unfortunately Gamescope does not seem to detect properly the screen
native resolution. Please edit the resolution by overriding the launch
command with `systemctl edit steam`.
Add the following (if you want 1440p):

```
[Service]
Environment=WIDTH=2560 HEIGHT=1440
```
