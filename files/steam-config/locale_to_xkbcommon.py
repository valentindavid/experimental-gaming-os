#!/usr/bin/env python3

import dbus

system_bus = dbus.SystemBus()

locale1 = system_bus.get_object('org.freedesktop.locale1',
                                '/org/freedesktop/locale1')
locale1_obj = dbus.Interface(locale1,
                             dbus_interface='org.freedesktop.DBus.Properties')

for var in ['layout', 'model', 'variant', 'options']:
    all_caps = var.upper()
    value = locale1_obj.Get('org.freedesktop.locale1', 'X11' + var.capitalize())
    if value:
        print(f'XKB_DEFAULT_{all_caps}={value};')
        print(f'export XKB_DEFAULT_{all_caps};')
    else:
        print(f'unset XKB_DEFAULT_{all_caps};')
