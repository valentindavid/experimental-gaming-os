#include "common.h"

#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <stdlib.h>

int forkexec(const char *file, char *const argv[]) {
  pid_t pid = fork();

  if (pid == 0) {
    execvp(file, argv);
    perror("execvp");
    exit(1);
  } else if (pid > 0) {
    int wstatus;
    do {
      if (waitpid(pid, &wstatus, 0) == -1) {
        perror("Error while waiting for process");
        exit(1);
      }
    } while (!WIFEXITED(wstatus));
    return WEXITSTATUS(wstatus);
  } else {
    perror("Cannot fork");
    exit(1);
  }
}
