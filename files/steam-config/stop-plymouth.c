#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>

#include "common.h"

int main() {
  int status;

  if (setuid(geteuid()) != 0) {
    perror("Cannot setuid");
    return 1;
  }

  char* const plymouth_ping[] = {"plymouth", "--ping", NULL};
  status = forkexec("plymouth", plymouth_ping);

  if (status != 0)
    return 0;

  char* const plymouth_show_splash[] = {"plymouth", "show-splash", NULL};
  status = forkexec("plymouth", plymouth_show_splash);
  if (status != 0) {
    fprintf(stderr, "Cannot run plymouth\n");
    return 1;
  }
  char* const plymouth_update[] = {"plymouth", "update", "--status=Starting Steam", NULL};
  status = forkexec("plymouth", plymouth_update);
  if (status != 0) {
    fprintf(stderr, "Cannot run plymouth\n");
    return 1;
  }

  char* const plymouth_deactivate[] = {"plymouth", "deactivate", NULL};
  status = forkexec("plymouth", plymouth_deactivate);
  if (status != 0) {
    fprintf(stderr, "Cannot run plymouth\n");
    return 1;
  }

  return 0;
}
