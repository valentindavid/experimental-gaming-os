#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

#include "common.h"

int main() {
  int status;

  if (setuid(geteuid()) != 0) {
    perror("Cannot setuid");
    return 1;
  }

  char* const plymouth_ping[] = {"plymouth", "--ping", NULL};
  status = forkexec("plymouth", plymouth_ping);

  if (status != 0)
    return 0;

  char* const plymouth_quit[] = {"plymouth", "quit", "--retain-splash", NULL};
  status = forkexec("plymouth", plymouth_quit);
  if (status != 0) {
    fprintf(stderr, "Cannot run plymouth\n");
    return 1;
  }

  return 0;
}
