#!/bin/bash

set -eux

rm -rf /var/lib/flatpak/*
rm -rf /var/tmp/*
rm -rf /tmp/*

flatpak remote-add flathub https://flathub.org/repo/flathub.flatpakrepo

flatpak install -y com.valvesoftware.Steam//stable                      \
        org.freedesktop.Platform.VulkanLayer.MangoHud//21.08            \
        com.valvesoftware.Steam.CompatibilityTool.Proton//stable        \
        com.valvesoftware.Steam.CompatibilityTool.Proton-GE//stable     \
        com.valvesoftware.Steam.CompatibilityTool.Proton-Exp//stable    \
        com.github.Matoking.protontricks                                \
        org.freedesktop.Platform.VulkanLayer.vkBasalt//21.08            \
        com.valvesoftware.Steam.CompatibilityTool.Boxtron//stable

flatpak override --system-talk-name=org.freedesktop.NetworkManager com.valvesoftware.Steam

tar -zc -C /var/lib/flatpak --owner=root:0 --group=root:0 -f /result/flatpak-with-steam.tar.gz .
